package com.example.demo.web.rest;

import com.example.demo.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/demo")
@RequiredArgsConstructor
public class HelloController {

    private final HelloService helloService;

    @PostMapping("/hello")
    private String sayHelloUser() {
        return helloService.sayHelloUser();
    }

    @GetMapping("/public")
    private String sayHelloPublic() {
        return helloService.sayHelloPublic();
    }
}
