package com.example.demo.service;

import com.example.demo.service.dto.UserAdminDto;
import com.example.demo.service.dto.UserDto;

public interface UserService {

    void createUser(Long ownerId, UserDto userDto);

    void createOwner(Long organizationId, UserAdminDto userAdminDto);
}
