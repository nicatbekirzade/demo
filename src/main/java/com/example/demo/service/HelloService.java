package com.example.demo.service;

public interface HelloService {

    String sayHelloUser();

    String sayHelloPublic();
}
