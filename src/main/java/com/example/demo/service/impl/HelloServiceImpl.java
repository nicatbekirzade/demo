package com.example.demo.service.impl;

import com.example.demo.service.HelloService;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {//http://localhost:9090/v1/demo/public

    @Override
    public String sayHelloUser() {
        return "Hello user";
    }

    @Override
    public String sayHelloPublic() {
        return "Hello everyone";
    }
}
