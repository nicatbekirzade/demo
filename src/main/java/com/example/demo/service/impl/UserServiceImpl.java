package com.example.demo.service.impl;

import com.example.demo.domain.Organization;
import com.example.demo.domain.Role;
import com.example.demo.domain.User;
import com.example.demo.exception.AlreadyExistException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.OrganizationRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import com.example.demo.service.dto.UserAdminDto;
import com.example.demo.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private static final String ADMIN = "ADMIN";
    private static final String USER = "USER";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ModelMapper modelMapper;
    private final OrganizationRepository organizationRepository;


    @Override
    @Transactional
    public void createUser(Long ownerId, UserDto userDto) {
        checkIfUserExistsByUserName(userDto.getUserName());
        checkIfUserExistsByEmail(userDto.getEmail());
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        User owner = findUserById(ownerId);
        User employee = modelMapper.map(userDto, User.class);
        Role userRole = getUserRole();
        employee.setActive(true);
        employee.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        employee.setOwner(owner);
        owner.getEmployees().add(employee);
    }

    @Override
    @Transactional
    public void createOwner(Long organizationId, UserAdminDto userAdminDto) {
        checkIfUserExistsByUserName(userAdminDto.getUserName());
        checkIfUserExistsByEmail(userAdminDto.getEmail());
        Organization organization = findOrganizationById(organizationId);
        userAdminDto.setPassword(bCryptPasswordEncoder.encode(userAdminDto.getPassword()));
        User owner = modelMapper.map(userAdminDto, User.class);
        organization.setOwner(owner);
        owner.setActive(true);
        Role userRole = getAdminRole();
        owner.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
    }

    private Organization findOrganizationById(Long organizationId) {
        return organizationRepository.findById(organizationId)
                .orElseThrow(() -> new NotFoundException(
                        String.format("Organization with: '%d' does not exist", organizationId)));
    }

    private void checkIfUserExistsByEmail(String email) {
        userRepository.findByEmail(email)
                .ifPresent(u -> {
                    throw new AlreadyExistException(
                            String.format("User by email: '%s' already exists", email));
                });
    }

    private void checkIfUserExistsByUserName(String userName) {
        userRepository.findByUserName(userName).ifPresent(u -> {
            throw new AlreadyExistException(String.format(
                    "User by user name : '%s' already exists", userName));
        });
    }

    private Role getUserRole() {
        Optional<Role> optionalRole = roleRepository.findByName(USER);
        if (optionalRole.isEmpty()) {
            Role role = new Role();
            role.setName(USER);
            return roleRepository.save(role);
        }
        return optionalRole.get();
    }

    private Role getAdminRole() {
        Optional<Role> optionalRole = roleRepository.findByName(ADMIN);
        if (optionalRole.isEmpty()) {
            Role role = new Role();
            role.setName(ADMIN);
            return roleRepository.save(role);
        }
        return optionalRole.get();
    }

    private User findUserById(Long ownerId) {
        return userRepository.findById(ownerId).orElseThrow(() -> new NotFoundException(
                String.format("User with id: '%d' does not exist", ownerId)
        ));
    }
}

