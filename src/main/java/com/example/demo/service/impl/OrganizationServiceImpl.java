package com.example.demo.service.impl;

import com.example.demo.domain.Address;
import com.example.demo.domain.Organization;
import com.example.demo.exception.AlreadyExistException;
import com.example.demo.repository.AddressRepository;
import com.example.demo.repository.OrganizationRepository;
import com.example.demo.service.OrganizationService;
import com.example.demo.service.dto.OrganizationDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public void createOrganization(OrganizationDto organizationDto) {
        Organization org = modelMapper.map(organizationDto, Organization.class);
        checkIfOrganizationExistsByName(organizationDto.getName());
        org.setAddress(modelMapper.map(organizationDto.getAddress(), Address.class));
        organizationRepository.save(org);
    }

    private void checkIfOrganizationExistsByName(String organizationName) {
        organizationRepository.findByName(organizationName).ifPresent(o -> {
            throw new AlreadyExistException(
                    String.format("Organization with name: '%s' already exists", organizationName));
        });
    }
}
