package com.example.demo.service.impl;

import com.example.demo.domain.Task;
import com.example.demo.domain.User;
import com.example.demo.domain.enums.TaskStatus;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.TaskRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.TaskService;
import com.example.demo.service.UserService;
import com.example.demo.service.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {


    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Override
    public void deleteTask(Long taskId) {
        Task taskById = findTaskById(taskId);
        taskRepository.delete(taskById);
    }

    @Override
    public List<TaskDto> findAllTasks() {
        return taskRepository.findAll().stream()
                .map(t -> modelMapper.map(t, TaskDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void createTask(Long ownerId, TaskDto taskDto) {
        User owner = findUserById(ownerId);
        Task task = modelMapper.map(taskDto, Task.class);
        task.setCreatedBy(owner.getUserName());
        task.setOwner(owner);
        taskRepository.save(task);
    }


    @Override
    public void updateTask(Long taskId, TaskDto taskDto) {
        Task task = findTaskById(taskId);
        task.setTitle(taskDto.getTitle());
        task.setDescription(taskDto.getDescription());
        taskRepository.save(task);
    }

    @Override
    public void setTaskCompleted(Long taskId) {
        Task task = findTaskById(taskId);
        task.setStatus(TaskStatus.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    public void setTaskInProgress(Long taskId) {
        Task task = findTaskById(taskId);
        task.setStatus(TaskStatus.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void assignTaskToUser(Long userId, Long taskId) {
        Task task = findTaskById(taskId);
        User owner = findUserById(userId);
        User selectedUser = findUserById(userId);
        task.setOwner(owner);
        task.getUsers().add(selectedUser);
        taskRepository.save(task);
    }

    private Task findTaskById(Long taskId) {
        return taskRepository.findById(taskId).orElseThrow(() -> new NotFoundException(
                String.format("Task with id: '%d' does not exist", taskId)));
    }

    private User findUserById(Long ownerId) {
        return userRepository.findById(ownerId).orElseThrow(() -> new NotFoundException(
                String.format("User with id: '%d' does not exist", ownerId)
        ));
    }
}
