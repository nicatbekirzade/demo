package com.example.demo.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
public class UserDto {

    @Size(min = 5, message = "Your user name must have at least 5 characters")
    @NotBlank(message = "Please provide a user name")
    private String userName;

    @Email(message = "Please provide a valid Email")
    @NotBlank(message = "Please provide your email")
    @ApiModelProperty(notes = "Your email should be in style of : example@example.com")
    private String email;

    @Size(min = 5, message = "Your password must have at least 5 characters")
    @NotBlank(message = "Please provide your password")
    private String password;

    @NotBlank(message = "Please provide your name")
    private String firstName;

    @NotBlank(message = "Please provide your lastname")
    private String lastName;

    private UserDto owner;

}
