package com.example.demo.service.dto;

import lombok.Data;

@Data
public class AddressDto {

    private String street;

    private String city;

    private String building;

    private OrganizationDto organization;
}
