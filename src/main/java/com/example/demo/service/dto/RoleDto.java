package com.example.demo.service.dto;

import lombok.Data;

@Data
public class RoleDto {

    private String role;
}
