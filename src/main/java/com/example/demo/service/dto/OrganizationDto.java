package com.example.demo.service.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OrganizationDto {

    @NotBlank(message = "Please provide your Organization name")
    private String name;

    @NotBlank(message = "Please provide your Organization phone number")
    private String phone;

    private AddressDto address;

}
