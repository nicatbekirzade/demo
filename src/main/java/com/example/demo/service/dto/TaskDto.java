package com.example.demo.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
public class TaskDto {

    @NotBlank(message = "Please provide a title for the task")
    private String title;

    @NotBlank(message = "Please provide a description for the task")
    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(notes = "Date  format is yyyy-MM-dd ")
    private LocalDate deadline;
}
