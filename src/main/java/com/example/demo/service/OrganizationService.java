package com.example.demo.service;

import com.example.demo.service.dto.OrganizationDto;

public interface OrganizationService {

    void createOrganization(OrganizationDto organizationDto);
}
