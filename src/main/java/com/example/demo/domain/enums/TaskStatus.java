package com.example.demo.domain.enums;

public enum TaskStatus {
    COMPLETED, IN_PROGRESS
}
